#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <map>
#include "building.h"
#include "squad.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots: // Слоты - функции, которые будут вызываеться, в том случае, если пришёл сигнал, к которому он подключен
    void buildingClicked(Building* click); //слот когда кликнули на звание
    void timeUpdate();                     //слот функции, котороя будет постоянно вызываться по таймеру
    void tryToFix();                       //слот функции, которая будет вызываться с некоторой задержкой, (через время, отряд попробует исправить проблему)
    void on_AssignSquadButton_clicked();   //слот нажатия на кнопку отправления отряда

private:
    Ui::MainWindow *ui;              //Указатель на основное окно
    Building* buildings;             //Массив зданий
    QList<FireSquad*> fireSquads;    //Список отрядов пожарных
    QList<GuardSquad*> guardSquads;  //Список отрядов охранников
    Building* currentBuilding;       //Указатель на выбранное сейчас здание

    std::map<Building*, Squad*> BSmap;  //<ключ, значение> для закрепления отряда за зданием, над которым они работают
    QList<Building*> fixList;           //Список зданий над которыми ведётся сейчас работа


};
#endif // MAINWINDOW_H
