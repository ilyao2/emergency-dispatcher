#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <string>
#include <random>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    srand( time(0) );
    //Инициализируем отряды и здания
    currentBuilding = nullptr;
    buildings = new Building[18];
    fireSquads.push_back(new FireSquad("alpha"));
    fireSquads.push_back(new FireSquad("betta"));
    fireSquads.push_back(new FireSquad("gamma"));

    guardSquads.push_back(new GuardSquad("alpha"));
    guardSquads.push_back(new GuardSquad("betta"));
    guardSquads.push_back(new GuardSquad("gamma"));

    ui->setupUi(this); //инициализация основного окна

    ui->toolPanel->setVisible(false); //Панель инструментов изначально скрыта
    //Добавляем отряды в список свободных отрядов для выбора
    for(int i = 0; i < 3; i++)
    {
        ui->freeSquads->addItem(fireSquads.value(i)->getItem());
    }
    for(int i = 0; i < 3; i++)
    {
        ui->freeSquads->addItem(guardSquads.value(i)->getItem());
    }

    //Связываются Сигналы Зданий со Слотами основного класса
    //А именно клик на здание с кликом на здание
    for(int i = 0; i < 18; i++)
    {
        ui->gridBuildingsLayout->addWidget(&buildings[i], i/6+1, i%6+1); // Добавляем здание на окно
        connect(&buildings[i], SIGNAL(Clicked(Building*)), this, SLOT(buildingClicked(Building*)));//Свзяываем сигналы и слоты
        //указатель на класс сигнала, метод сигнала, указатель на класс приёмника, слот приёмника
    }

    QTimer *timer = new QTimer(this);                           //Создаём таймер
    connect(timer, SIGNAL(timeout()), this, SLOT(timeUpdate()));//Соединяем сигнал таймера со слотом апдейт
    timer->start(5000);                                         //Запускаем таймер(Вызывается каждые 5 секунд)

}

MainWindow::~MainWindow()
{
    // Главное не забыть освободить всю выделенную память операторами new
    if(buildings != nullptr)
        delete [] buildings;
    foreach(FireSquad* temp, fireSquads)
        delete temp;
    foreach(GuardSquad* temp, guardSquads)
        delete temp;
    fireSquads.clear();
    guardSquads.clear();
    delete ui;
}

void MainWindow::buildingClicked(Building* click)
{
    currentBuilding = click;                                                  // Текущим становится здание на которое кликаем
    ui->NumOfBuildings->setText(std::to_string(click->getNumber()).c_str());  // Просто вывод номера ЗДания(itoa())

    // Вывод статус здания
    if(click->getStatus() == BUILDING_STATUS::NORMAL)
        ui->TypeOfEmer->setText("NONE");
    else if(click->getStatus() == BUILDING_STATUS::FIRE)
        ui->TypeOfEmer->setText("On Fire");
    else if(click->getStatus() == BUILDING_STATUS::CRIME)
        ui->TypeOfEmer->setText("Crime");

    //Вывод названия отправленного в это здание отряда
    Squad* squad = BSmap[click]; // если здание не связано с отрядом будет nullptr
    if(squad != nullptr)
        ui->AssignedSquad->setText(squad->getName());
    else ui->AssignedSquad->setText("NONE");

    ui->toolPanel->setVisible(true);// Делаем панель инструментов видимой

    ui->statusbar->clearMessage();

}
void MainWindow::timeUpdate()
{
    int m = rand()%18; //Случайное из 18и Здание
    int s = rand()%3;  //Случайное проишествие 0 - пожар, 1 - ограбление, 2 - всё ок
    if(s == 0)buildings[m].Burn();
    else if(s == 1)buildings[m].Rob();

}

void MainWindow::tryToFix()
{
    Building* building = fixList.first(); // Берём первое в списке на исправление здание
    fixList.pop_front();                  // Удаляем его из этого списка
    Squad* squad = BSmap[building];       // Получаем отряд отправленный в это здание
    if(squad!=nullptr)
    {
        if(!squad->Fix(building) && building->getStatus() != BUILDING_STATUS::NORMAL) building->Up(); //Исправить проблему или Если отряд не может исправить проблему, привлечь внимание
        else building->Down();
        squad->Free();                            // Освободить отряд
        BSmap.erase(building);                    // Отвязать отряд от здания
        squad->getItem()->setHidden(false);       // Показать отряд в списке свободных
    }
    if(currentBuilding == building)
    {
        ui->statusbar->clearMessage();
        // Вывод статус здания
        if(building->getStatus() == BUILDING_STATUS::NORMAL)
            ui->TypeOfEmer->setText("NONE");
        else if(building->getStatus() == BUILDING_STATUS::FIRE)
            ui->TypeOfEmer->setText("On Fire");
        else if(building->getStatus() == BUILDING_STATUS::CRIME)
            ui->TypeOfEmer->setText("Crime");

        ui->AssignedSquad->setText("NONE");
    }
}


void MainWindow::on_AssignSquadButton_clicked()
{
    ui->statusbar->clearMessage();
    //Проход по всем отрядам
    for(int i = 0; i < 3; i++)
    {
        if(ui->freeSquads->selectedItems().contains(fireSquads.value(i)->getItem())) // Если нашли тот, который выбран в списке
        {
            Squad* temp = fireSquads.value(i);                                       // Для удобства
            if(BSmap[currentBuilding] == nullptr)                                     // Если в это здание ещё не отправлен отряд
            {
                BSmap[currentBuilding] = temp;                                           // Связываем отряд с текущим зданием
                temp->Direct();                                           // Отправить его
                temp->getItem()->setHidden(true);                         // Скрываем отряд из списка свободных
                fixList.push_back(currentBuilding);                                      // Добавляем текущее здание в конец списка зданий над которыми ведётся работа
                currentBuilding->Work();
                QTimer::singleShot(3000, this, SLOT(tryToFix()));                        // Вызываем отложенный на 3 секунды метод для попытки исправить проблему
                //синглшот - единожды выполнится через 3 сек. Этот класс приёмник, слот этого класса tryToFix
                //по сути связывает сигнал таймера со слотом вызова
                //Здесь можно настроить время проведения работ


                ui->AssignedSquad->setText(temp->getName());
            }
            else
            {
                ui->statusbar->showMessage("В это здание отряд уже отправлен");
            }
        }
        //Всё тоже самое для дрого типа отрядов
        else if(ui->freeSquads->selectedItems().contains(guardSquads.value(i)->getItem()))
        {
            Squad* temp = guardSquads.value(i);
            if(BSmap[currentBuilding] == nullptr)
            {
                BSmap[currentBuilding] = temp;
                temp->Direct();
                temp->getItem()->setHidden(true);
                fixList.push_back(currentBuilding);
                currentBuilding->Work();
                QTimer::singleShot(3000, this, SLOT(tryToFix()));

                ui->AssignedSquad->setText(temp->getName());
            }
            else
            {
                ui->statusbar->showMessage("В это здание отряд уже отправлен");
            }
        }
    }
}
