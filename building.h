#ifndef BUILDING_H
#define BUILDING_H
#include <QLabel>
#include <QPixmap>
#include <QEvent>
//Константы для удобства
namespace BUILDING_STATUS {
const int NORMAL = 0;
const int FIRE = 1;
const int CRIME = 2;
}

class Building : public QLabel         //Наследуется от лэйбла, чтобы помещать на интерфейс
{
    Q_OBJECT                           //необходимый для qt макрос для всех наследников QObject
signals:                               //Сигналы - методы, которые вызываются в определённый моменты, аналог делигатов
    void Clicked(Building* click);     //Подавать сигнал о том, что на лайбыл кликнули
private:
    int Number;                      //номер дома
    int Status;                      //статус
    static int idCounter;            //переменная для создания уникальных номеров
    QPixmap normalPix;               //картинка в нормальном состоянии
    QPixmap upPix;                   //картинка, когда требуется внимание
    QPixmap firePix;
    QPixmap crimePix;
    QPixmap timePix;

    bool operator< (const Building& a) const //нужно перегружать этот оператор для нормальной работы std::map
    {
         return this->Number < a.Number;
    }
public:
    explicit Building(QWidget* parent = nullptr); //Явный конструктор  (Building b = parent не будет работать)
    bool Burn();                     //Поджечь дом
    bool Rob();                      //Грабить дом
    void Fix();                      //Исправить все проблемы
    void Up();                       //Привлеч внимание диспетчера
    void Down();
    void Work();
    int getStatus(){return Status;}
    int getNumber(){return Number;}

protected:
    void mousePressEvent(QMouseEvent* event); //Делигат, вызывается после клика мышкой
    using QLabel::QLabel;
};

#endif // BUILDING_H
