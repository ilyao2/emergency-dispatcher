#include "squad.h"

Squad::Squad(QString name)
{
    Name = name;
    Item = new QListWidgetItem;
    Item->setText(Name);
    Status = SQUAD_STATUS::FREE;
}
Squad::Squad()
{
    Name = "*";
    Item = new QListWidgetItem;
    Item->setText(Name);
    Status = SQUAD_STATUS::FREE;
}
Squad::~Squad()
{

}

void Squad::Free()
{
    Status = SQUAD_STATUS::FREE;
}

void Squad::Direct()
{
    Status = SQUAD_STATUS::BUSY;
}

bool FireSquad::Fix(Building *obj)
{
    if(obj->getStatus() == BUILDING_STATUS::FIRE)
    {
        obj->Fix();
        return true;
    }
    return false;
}

bool GuardSquad::Fix(Building *obj)
{
    if(obj->getStatus() == BUILDING_STATUS::CRIME)
    {
        obj->Fix();
        return true;
    }
    return false;
}

