#include "building.h"
int Building::idCounter = 0;

Building::Building(QWidget* parent):QLabel(parent) //Конструктор
{
    Number = idCounter;
    idCounter++;
    Status = BUILDING_STATUS::NORMAL;
    normalPix.load("house.png");  //загрузка картинок
    upPix.load("houseUp.png");
    firePix.load("fire.png");
    crimePix.load("guard.png");
    timePix.load("time.png");
    this->setPixmap(normalPix);  //установка картинки
}

bool Building::Burn()
{
    if(Status == BUILDING_STATUS::NORMAL)
    {
        Status = BUILDING_STATUS::FIRE;
        setPixmap(firePix);
        return true;
    }
    return false;
}
bool Building::Rob()
{
    if(Status == BUILDING_STATUS::NORMAL)
    {
        Status = BUILDING_STATUS::CRIME;
        setPixmap(crimePix);
        return true;
    }
    return false;
}

void Building::Fix()
{
    Status = BUILDING_STATUS::NORMAL;
}


void Building::mousePressEvent(QMouseEvent* event)
{
    emit Clicked(this);   //Послать сигнал о том, что на зданее клиикнули с передачей ссылки на это здание
}
void Building::Up()
{
    setPixmap(upPix);
}

void Building::Down()
{
    setPixmap(normalPix);
}
void Building::Work()
{
    setPixmap(timePix);
}
