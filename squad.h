#ifndef SQUAD_H
#define SQUAD_H
#include <QString>
#include <QListWidgetItem>
#include "building.h"
//Константы для удобства
namespace SQUAD_STATUS {
    const int FREE = 0;
    const int BUSY = 1;
}
class Squad
{
protected:
    int Status;                //Статус
    QString Name;              //Название
    QListWidgetItem* Item;     //Айтем для отображения в списке
public:
    Squad(QString name);
    Squad();
    virtual ~Squad(); //Деструктор должен быть виртуальным, если класс абстрактный
    void Direct();    //Отправить отряд
    void Free();      //Освободть отряд
    virtual bool Fix(Building* obj) = 0;   //Виртуальная функция, обязательно перегружается в наследниках(исправить проблему здания)
    int getStatus(){return Status;}
    QString getName(){return Name;}
    QListWidgetItem* getItem(){return Item;}
};

class FireSquad : public Squad
{
public:
    FireSquad()
    {
        FireSquad("*");
    }
    FireSquad(QString name) : Squad() //В конструкторе перед именем добавляется тип отряда
    {
        Name = "FireSquad: " + name;
        Item = new QListWidgetItem();
        Item->setText(Name);
    }
    ~FireSquad(){};                 //перегрузка деструктора
    bool Fix(Building *obj);        //будет фиксить только дома с пожаром
};

class GuardSquad : public Squad
{
public:
    GuardSquad() : Squad()
    {
        GuardSquad("*");
    }
    GuardSquad(QString name) : Squad()
    {
        Name = "GuardSquad: " + name;
        Item = new QListWidgetItem();
        Item->setText(Name);
    }
    ~GuardSquad(){};
    bool Fix(Building *obj);        //будет фиксить только дома, которые грабят
};

#endif // SQUAD_H
